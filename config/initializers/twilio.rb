Twilio.configure do |config|
    config.account_sid = Rails.application.secrets.twilo_account_id
    config.auth_token = Rails.application.secrets.twilo_auth_token

end
