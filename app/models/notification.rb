class Notification < ApplicationRecord
    after_create_commit { NotificationJob.perfom_later self}
  belongs_to :user
end
